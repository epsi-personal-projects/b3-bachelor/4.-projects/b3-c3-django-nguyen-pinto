# Install Development Environment
```
python -m pip install Django
apt install sqlite3
```

# Run Development Server
```
python manage.py runserver
```

# Database Migrations / Refresh
```
python manage.py makemigrations "module"
```
```
python manage.py migrate
```

# Create Superuser
```
python3 manage.py createsuperuser
```