from django import forms
from django.http import HttpRequest, HttpResponse, HttpResponsePermanentRedirect, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout as dca_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm

from reservation.models import Reservation
from django.shortcuts import render
from reservation.models import School, Event, User

# Create your views here.

# 2
@login_required
def index(request):
    schools:list[School] = School.objects.all()
    return render(request, 'index.html', {"schools": schools})

# 3
@login_required
def reservations(request: HttpRequest) -> HttpResponse:
    """render the list of user's reservations

    Args:
        request (HttpRequest): request from the frontend

    Returns:
        HttpResponse: return html page
    """
    user_id = request.user.id
    reservations = Reservation.objects.filter(user=user_id)
    
    return render(request, 'reservations.html', {'reservations' : reservations})

@login_required
def reservation(request: HttpRequest, id: int) -> HttpResponse:
    """manage reservation accord to the http request method

    Args:
        request (HttpRequest): request from the frontend
        id (int): reservation id

    Returns:
        HttpResponse: return status code result
    """
    if request.method == 'POST':
        user = User.objects.filter(id=request.user.id).first()
        event = Event.objects.filter(id=id).first()

        new_reservation = Reservation(user = user, event = event)
        new_reservation.save()
        
        return HttpResponse(status=201)
    elif request.method == 'DELETE':
        Reservation.objects.filter(id=id).delete()
        messages.error(request,'Réservation supprimée avec succès !')
        return HttpResponse(status=204)
    
    return HttpResponse(status=400)
    


# 4
@login_required 
def event_list(request, school_id):
    """render the list of the events of a specific school

    Args:
        request (HttpRequest): request from the frontend
        school_id (Number)

    Returns:
        HttpResponse: return html page
    """
    events = Event.objects.all().filter(school=school_id)
    return render(request, 'events.html', {"events": events})

# 1
def auth(request: HttpRequest) -> HttpResponseRedirect | HttpResponsePermanentRedirect | HttpResponse:
    """mange user authentication

    Args:
        request (HttpRequest): request from the frontend

    Returns:
        HttpResponseRedirect | HttpResponsePermanentRedirect | HttpResponse: return html page
    """
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.error(request,'Pseudo ou mot de passe incorrect')
            return redirect('auth')
    
    else:
        form = LoginForm()
        return render(request, 'auth.html', {'form': form})

@login_required 
def logout(request: HttpRequest) -> HttpResponseRedirect | HttpResponsePermanentRedirect:
    """mange user logout

    Args:
        request (HttpRequest): request from the frontend

    Returns:
        HttpResponseRedirect | HttpResponsePermanentRedirect: return html page
    """
    dca_logout(request)

    return redirect('auth')

class LoginForm(AuthenticationForm):
    """manage the login form, it extends AuthenticationForm from Django authentication to custom attribut
    """
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Pseudo'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Mot de passe'}))
    
def my_custom_page_not_found_view(request: HttpRequest, exception) -> HttpResponse:
    """Manage 404 status code, DEBUG has to be set to False to see the 404.html 

    Args:
        request (HttpRequest): request from the frontend
        exception (_type_): not used

    Returns:
        HttpResponse: return html page
    """
    return render(request, '404.html')