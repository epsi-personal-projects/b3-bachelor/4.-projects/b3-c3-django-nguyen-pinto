from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('auth/', views.auth, name='auth'),
    path('logout/', views.logout, name='logout'),
    path('reservations/', views.reservations, name='reservations'),
    path('reservations/<int:id>', views.reservation, name='reservation'),
    path('events/<int:school_id>/', views.event_list, name='events'),
]

handler404 = 'reservation.views.my_custom_page_not_found_view'
