from django.contrib import admin
from .models import User, School, Event, Reservation

# Register your models here.
admin.site.register(User)
admin.site.register(School)
admin.site.register(Event)
admin.site.register(Reservation)