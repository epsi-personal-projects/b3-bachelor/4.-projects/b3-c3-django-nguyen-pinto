from django.db import models

# Create your models here.
class User(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    
class School(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField()
    
class Event(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    date = models.DateTimeField()
    
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    
class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
